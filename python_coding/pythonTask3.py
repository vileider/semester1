# python task from https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list/problem?isFullScreen=true
# I am coping the code from hakkerrank and starting my first attempt
# I created random number between 4 and 9 as array length, and I inject random variables between 1,10 into that array.
import random

if __name__ == '__main__':

    n = int(random.randint(4, 9))
    arr = []
    for x in range(n):
        arr.append(int(random.randint(1, 10)))


# Attempt 1
# I looked for python sorting methods and I found one at https://docs.python.org/3/howto/sorting.html

# print(n)
# print(arr)
# arr.sort()
# print(arr)


# Attempt 2
# I decided to sort the list from smallest value to biggest one at first.
#  When I had Array sort compare highest array index with next lower one and so on

arr.sort()
n = n-1

if arr[n] == arr[-1]:
    while arr[n] == arr[-1]:
        n = n-1
else:
    print(arr[n-1])
print(arr[n])
