# https://www.hackerrank.com/challenges/list-comprehensions/problem?isFullScreen=true
# I am copying code from hackerrank and creating random int that will imitate input.
# I learned that I need to import "random" module
# at the beginning I tried to check if every variable display as it should

import random

if __name__ == '__main__':
    x = int(random.randint(0, 1))
    y = int(random.randint(0, 1))
    z = int(random.randint(0, 1))
    n = int(2)


# attempt 1

# for x in range(n):
#     print(x, y, z, n)


# attempt 2
# when I write code like this I receive error:
# UnboundLocalError: local variable 'y' referenced before assignment

# print([[x, y, z] for x in [x, y, z] for y in [x, y, z]])


# attempt 3
# I created trioSum for better readability
# finally I received output similar to expected one. I just need to get that output for x , y and z.
#trioSum = x+y+z

# print([[x, y, z] for x in [x, y, z] if trioSum != n])


# attempt 4
# I need to eliminate variables that repeats

# print([[x, y, z] for x in [x, y, z] if trioSum != n] + [[x, y, z]
#       for y in [x, y, z] if trioSum != n] + [[x, y, z] for z in [x, y, z] if trioSum != n])


# attempt 5
# I am trying different approach to a problem because error from attempt number 2 appears
# also I want to see what numbers are created every time

print(x, y, z, n)
# results = []
# for x in [x, y, z]:
#     for y in [x, y, z]:
#         for z in [x, y, z]:
#             results.append([x, y, z])
# print(results)


# attempt 6
# I just realized that trioSum variable was a bad idea because I need to check the condition locally, nested in a loop
# I reread the task and I noticed that x,y,z should be in range 0 to x,y,z

results = []
# for x in range(0, x):
#     for x in [x, y, z]:
#         for y in range(0, y):
#             for y in [x, y, z]:
#                 for z in [x, y, z]:
#                     if x+y+z != n:
#                         if [x, y, z] not in results:
#                             results.append([x, y, z])
# print(results)


# attempt 7

# resultsForX = []
# for x in range(0, x):
#     for x in [x, y, z]:
#         if [x, y, z] not in resultsForX:
#             resultsForX.append([x, y, z])

# for y in range(0, y):
#     for y in [x, y, z]:
#         if [x, y, z] not in resultsForX:
#             resultsForX.append([x, y, z])

# for z in range(0, z):
#     for z in [x, y, z]:
#         if [x, y, z] not in resultsForX:
#             print([x, y, z])
#             resultsForX.append([x, y, z])


# attempt 8
# I tried to use separate in range for x ,y and z but it did not work.
# I was still missing couple values

# resultsX = []
# resultsY = []
# resultsZ = []
# for x in range(0, x):
#     resultsX.append([[x, y, z] for x in [x, y, z]])
# print(resultsX)
# for y in range(0, y):
#     resultsY += [[x, y, z] for y in [x, y, z]]
# print(resultsY)
# for z in range(0, z):
#     resultsZ += [[x, y, z] for z in [x, y, z]]
# print(resultsY)

# results = resultsX + resultsY + resultsZ


# attempt 9

# This one work but does not display values in correct order
# newResults = []
# for x in range(0, x+1):
#     for y in range(0, y+1):
#         for z in range(0, z+1):
#             results += ([[x, y, z] for z in [x, y, z]
#                         if (x+y+z != n)])

# for x in range(len(results)):
#     if results[x] not in newResults:
#         newResults.append(results[x])
# print(results)
# print(newResults)


# attempt 10
# finally works and shows values in correct order

results = []
for x in range(0, x+1):
    for y in range(0, y+1):
        for z in range(0, z+1):
            if x + y+z != n:
                results.append([x, y, z])
print(results)
