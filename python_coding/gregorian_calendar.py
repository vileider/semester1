# attempt 1
# def is_leap(year):
#     leap: True

#     if(year % 4 == 0 & year % 100 != 0):
#         if(year % 400 == 0):
#             leap = True
#         else:
#             leap = False
#     else:
#         leap: True

#     return leap


# year = int(1992)
# print(is_leap(year))

# attempt 2
# def is_leap(year):
#     leap = False
#     yearD = 4
#     if(year % 4 == 0 & year % 100 != 0):
#         if(year % 400 == 0):
#             leap = True
#             yearD = year % 400
#         else:
#             leap = False
#             yearD = year % 400
#     else:
#         leap: True
#         yearD = year % 4

#     return (leap, yearD)


# year = int(1992)
# print(is_leap(year))

# attempt 3
def is_leap(year):
    leap = False
    if(year % 4 != 0):
        leap = False
    else:
        if(year % 100 != 0):
            leap = True
        else:
            if(year % 400 == 0):
                leap = True
            else:
                leap = False

    return (leap)


year = int(1992)
print(is_leap(year))
