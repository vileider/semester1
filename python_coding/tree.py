class TreeNode:
    def __init__(self, data):
        self.data = data
        self.children = []
        self.parent = None

    def add_child(self, child):
        child.parent = self
        self.children.append(child)

    def get_level(self):
        """
        Checking level of tree element
        """
        level = 0
        p = self.parent
        while p:
            level += 1
            p = p.parent

        return level

    def print_tree(self):
        """
        Recursive function to print tree elemnts
        """
        spaces = " " * self.get_level() * 3
        prefix = spaces + "|__" if self.parent else ""
        print(prefix + self.data)
        if self.children:
            for child in self.children:
                child.print_tree()


def build_product_tree():
    root = TreeNode("Electronics")

    laptop = TreeNode("Laptop")
    laptop.add_child(TreeNode("Mac"))
    laptop.add_child(TreeNode("Surface"))
    laptop.add_child(TreeNode("Think-pad"))

    cellphone = TreeNode("Cell Phone")
    cellphone.add_child(TreeNode("iPhone"))
    cellphone.add_child(TreeNode("Samsung"))
    cellphone.add_child(TreeNode("HTC"))

    tv = TreeNode("TV")
    tv.add_child(TreeNode("JVC"))
    tv.add_child(TreeNode("Hisense"))

    root.add_child(laptop)
    root.add_child(cellphone)
    root.add_child(tv)

    return root


if __name__ == "__main__":
    root = build_product_tree()
    # print(root.get_level())
    root.print_tree()

pass

"""
PS C:\school\repos\semester1\python_coding> python .\tree.py
Electronics
   |__Laptop
      |__Mac
      |__Surface
      |__Think-pad
   |__Cell Phone
      |__iPhone
      |__Samsung
      |__HTC
   |__TV
      |__JVC
      |__Hisense
"""
