// I will try to code the equivalent of a pythonTask2 https://bitbucket.org/vileider/semester1/src/main/python_coding/pythonTask2.py
// Task:  Print a list of all possible coordinates given by x,y,z where sum of x+y+z can not be equal n. coordinates start from 0 till randomized number
// I prepared random x,y,z coordinates

const n = 2;
const x = Math.floor(Math.random() * 2);
const y = Math.floor(Math.random() * 2);
const z = Math.floor(Math.random() * 2);

//attempt 1
// I tried to set up loops to fill secondList with firstList permutation
// but at the beginning I created a frame
// const uniqeColors = [...new Set(colors)]

const firstList = [0, 0, 0];

// console.log(firstList);
const secondList = [];

//   for (let i = 0; i < firstList.length; i++) {
//     console.log("array position iteration", i);
//     secondList.push(firstList);

// }

// console.log(secondList);

//attempt 2
// i am still missing case when first index =0

const memoOfXYZ = [x, y, z];
// for (let i = 0; i < firstList.length; i++) {
//   for (let a = 0; a <= x; a++) {
//     for (let a = 0; a <= y; a++) {
//       if (firstList[i] < memoOfXYZ[i]) {
//         firstList[i] = a;
//       }
//       secondList.push(firstList.slice(0));
//     }
//   }
// }

//attempt 3
//i had to nest 3 loops to achieve permutations
//however the second list contains identical arrays
//to not push reference to the same array into secondList i use .slice(), I could use method .concat() as well
// To erase duplicates from array I used method 'Set' from https://levelup.gitconnected.com/7-ways-to-remove-duplicates-from-array-in-javascript-cea4144caf31
//it is working for string so I changed my array of numbers into string and erased duplicates
//after erasing I duplicates I converted array of strings back numbers
//to pick x+y+y not equal n i use map method and filter results to get rid of "false" in return

for (let i = 0; i < memoOfXYZ.length; i++) {
  for (
    let firstList = [0, 0, 0];
    firstList[i] <= memoOfXYZ[i];
    firstList[i]++
  ) {
    for (let b = i; b <= memoOfXYZ.length; b++) {
      secondList.push(firstList.slice(0));
      firstList[b] < memoOfXYZ[b] && firstList[b]++;
    }
  }
}

const secondListWithOrigainalValues = [
  ...new Set(secondList.map((x) => JSON.stringify(x))),
].map((x) => JSON.parse(x));

const TaskSolution = secondListWithOrigainalValues
  .map((x) => x[0] + x[1] + x[2] !== n && x)
  .filter((x) => x !== false);

console.log(TaskSolution);
