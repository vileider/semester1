//I is equivalent of python task from https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list/problem?isFullScreen=true
// resolved here: https://bitbucket.org/vileider/semester1/src/main/python_coding/pythonTask3.py
//I am creating random variable between 5 and 10
const n = Math.floor(Math.random() * 6) + 5;
function newNumber() {
  return Math.floor(Math.random() * 6) + 5;
}
const listofnumbers = [];
for (let i = 0; i < n; i++) {
  listofnumbers.push(newNumber());
}

//attempt 1
//to sort the numbers in JavaScript i use sort function with parameters i found the solution at:
//https://www.w3schools.com/jsref/jsref_sort.asp

listofnumbers.sort((a, b) => a - b);

console.log(listofnumbers);

//attempt 2
//Now I just need to find the number not equal to highest one
//in my opinion easies way is to do it with do-while loop
let i = n - 1;
do {
  i--;
  listofnumbers[i] < listofnumbers[n - 1] && console.log(listofnumbers[i]);
} while (listofnumbers[n - 1] === listofnumbers[i]);
