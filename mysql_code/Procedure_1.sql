select * from userScore;

DELIMITER //
CREATE PROCEDURE show_all_users()
BEGIN
SELECT * FROM userScore;
END//
DELIMITER ;

CALL show_all_users();

DELIMITER //
CREATE PROCEDURE show_all_users_with_score(
IN expected_score INT
)
BEGIN
SELECT * FROM userScore
WHERE userScore = expected_score;
END//
DELIMITER ;

CALL show_all_users_with_score(1);

DELIMITER //
CREATE PROCEDURE add_user_and_score(
IN new_user VARCHAR(45),
IN new_score INT
)
BEGIN
INSERT INTO userScore (userName, userScore)
VALUES (
new_user,
new_score);
SELECT * FROM userScore LIMIT 5;
END//
DELIMITER ;

SELECT * FROM userScore ORDER BY userID DESC LIMIT 5 OFFSET 2;
CALL add_user_and_score('dude69',2);
select * from userScore ORDER BY userId DESC;
<---! order by userID DESC -this line was written with a feedback from one of the student --->