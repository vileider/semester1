Securing databases, webs, server, and single computer as a part of bigger internet infrastructure is important to prevent data loss, espionage or system malfunction.(LO1.1.1.4)
When I want to secure any software or operation system I should start from the basis. I shall not create password easy to guess. Any login details like credentials or saved password if store should be encrypted. When I am securing my application, I should use ready - made solutions. There are plenty of frameworks which will automatically encrypt secure data sent from client to server. Of course, it is worth checking if current framework has got trust of internet community.
When I secure my application should not prevent from any available attack in the world. I should focus to secure it good enough, that I would not be profitable brake-in in relation to the costs incurred.
I should learn from big companies. For them most important thing in cyber security is detection.
When big companies care about extra protection, they provide technical enforcement. The operation system of their employees are blocking installation of harmful or not so secure software. Sometimes they are not even the administrators of their own devices for the sake of their own security.
There are security solution like honeypots. Honeypots are decoy, they are mimicking real system.
It lets to observe real attacker behaviour and learn attack methods to protect real network.
Today, when every institution like hospitals, schools, road infrastructure or factories is a part of bigger network. Providing Antivirus security, spyware system, strong password, updating operation system, knowledge about unnecessary programs is mandatory to keep everyone’s life in safe. Every device must have at least minimum of security

### I am referring to:

- notes from Rik Ferguson (Trend Micro), Cyber security 20.11.2021 - guest lecture
- [here](../certificates/Node.js_Security.pdf)
- [here](../certificates/Security_Frameworks.pdf)
- [here](../certificates/Securing_REST.pdf)
