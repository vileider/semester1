Explain what is meant by the term “insider”, examining the security implications on an organisation’s network, system and data.LO1.3.1.4

As my team created a threat model document. I was able to look closely
on potential threats coming from inside the project.
We discussed that(1.3.1.4) new member in our team could add some changes to a code and break the whole
application by pushing changes to repository. That could be even done not intentional.
Any changes in our team should be done by sending pull request.
Unfortunately, our team is not using pull request yet, however we secure our self by keeping
the production on a different branch. 
Other potential threat could come from espionage. Anyone who has access to repository
is able to steal Team's Intellectual property. I think I would solve that problem 
by dividing repositories and each group/developer would see just small part of the whole project.
Facing the threat from inside is the worst possible option in my opinion. I am hoping to learn 
other ways to protect company in the future.

Bitbucket.org. 2022. Live Chat Team. [online] Available at: <https://bitbucket.org/JuliaKloda/live_chat/src/DEV/team_excercise/threat_model.md> [Accessed 1 March 2022].