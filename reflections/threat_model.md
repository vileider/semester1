Explain what a threat model is in a business context, which includes documenting what information is at risk, analysing the type and level of risk realised; and the impact of the risk being realised.
For this learning outcome you must be able to demonstrate that you can idenfity what a threat model is, document the threats including level and impact of risk in a business context. LO1.1.3.2

Our team has gathered after daily meeting with the intention to create document. 
The Document was intended to contain possible threats (LO1.1.3.2) at live chat project.
We divided the document into four, as a 4 main weak spots of our project(Live Chat Team, 2022).
(Live Chat Team, 2022)We named them:
* Sensitive data
* Internal threat from existing member and new team members
* External threats for database and server
* External threats for frontend.
One person mentioned that DDos attacks can be an issue. I should search the internet how to defend from it.
Other said that new member could cause a threat even unintentionally. It is showing that I should be aware of threat on many different levels. Thanks to group work I could see different point of view on that topic.
While I move forward with our project I should check if my actions can cause threats. I should not hesitate to consult my concerns about potential threats with team members, because looking at the same topic from the different angle could be beneficial.

Bitbucket.org. 2022. Live Chat Team. [online] Available at: <https://bitbucket.org/JuliaKloda/live_chat/src/DEV/team_excercise/threat_model.md> [Accessed 1 March 2022].