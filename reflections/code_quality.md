

I have been working under multiple project and I was thinking how can I 
improve code quality.(LO1.2.1.4)

Before I approach to this reflection I wrote user interface tests(Todorow B. and Kłoda J. 2022)
for team project. Our team had a code review, and I read article about quality of code.
I wrote some notes from this article.(Todorow B 2022)

When I was writing UI tests in testcafe automation tool I learned how to write simple tests.
I understand more the code structure of tests and how to run a test from a script in confing.json.
It is much easier to run one test to check functionality than to check manually every button, input field.
After every team-project code review I am learning how to improve my code readability
by instant feedback from other students. Our team should invite Tom or Karen. Their feedback should
improve quality of code. 
The article I read, taught me that there are other ways to improve code quality. I barely heard about
automated code review tools and I did not know that maintainability has such an impact on application.

While I was using testcafe I did some research. I discover that the most popular testing framework for
test-driven development is called Jest. I should read more about TDD and how use Jest.
Reading more articles about quality and code in general could guide me a bit. 

Improving code quality save my time thanks to better readability or test. I should spend extra
minutes on code improvement to save hours in the future.


Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/other_notes/code_quality_article.md> [Accessed 6 March 2022].

Todorow, B. and Kłoda, J., 2022. Log in with Atlassian account. [online] Bitbucket.org. Available at: <https://bitbucket.org/JuliaKloda/live_chat/src/test_our_project/react-chat-app/src/tests/darkmode.js> [Accessed 6 March 2022].