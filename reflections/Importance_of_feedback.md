
Receiving and producing feedback brings me closer to environment in professional IT company.
I will write what progress I did in that matter.(LO1.1.2.3)

I have a habit to invite my friend/ mentor to check my code, since I was taking first steps in computer programming. I was asking for feedback every time I was feeling I reached a milestone in my progress.
I was familiar with inviting others to peek on my code.

Once live chat team reached some level of functionality we invited Karen and Tom.
Their feedback was enlightening. I learned that code formatting is important for readability.
Tom explained that functionality which is not common should be preceded by descriptions.
(Todorow, React message component, 2022)
Over a period of several months I asked two students for feedback.
I received feedback from First student. He helped me to improve my MYSQL code.
Thanks to his advice I improve stored procedure and output was more precisely structured.
(Todorow, Stored procedures 2022)  
Second student look at one of my private project files. I have been told that 
Code look formatted and clean. I was happy about that I took Tom's lesson seriously.
Thanks to Toms feedback at code review I improved my work. Only two things were bothering my reviewer.
First, the arrow function- which is es6 feature so not everyone, get used to this syntax.
Second concerns was about need of refactoring. Code should be divided and separated in to reusable components.
I have put it on my TO DO list.(Todorow and Basiul, 2022)

I was helping many students to solve the programming problems. Unfortunately I do not put much attention
to what problem I am solving. I asked one of my team member if any feedback is needed.
I have been asked to review SQL code with stored procedures. I said that stored procedure looks fine, for a training purpose. Best way to use it is when query have to be long. Stored procedure shrinks big amount of lines and when used with parameters becomes more effective. Team member was happy to learn new things and thanked me for my feedback.

I learned that others point of view helps me to see details or bigger issues that I missed.
Helping other improves my self-confidence and create good relations between co-workers.


At first, I am sure I need to keep writing down what problem I help to solve.
That would not be just a written solution but also good log for me in case I would like to backtrack something.

I am trying to create a net of friends and students to communicate with each other.
We could rely one on another while solving programming problems or gaining knowledge about
the newest programming features. In next couple weeks I will be actively working towards this idea.


Todorow, B., 2022. Stored procedures. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/mysql_code/Procedure_1.sql> [Accessed 28 April 2022].

Todorow, B., 2022. React message component. [online] Bitbucket.org. Available at: <https://bitbucket.org/JuliaKloda/live_chat/src/master/react-chat-app/src/components/MessageDisplay.js> [Accessed 28 April 2022].

Todorow, B. and Basiul, S., 2022. Login Component. [online] GitHub. Available at: <https://github.com/szymonbasiul/bosoreact2/blob/Szymon_branch/src/appDivComponents/projects/memory_game/memoryComponents/RegisterLogin.tsx> [Accessed 28 April 2022].