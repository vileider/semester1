

At the end of the sprint Our team decided to have a code review.(Live chat team 2022)
My team had a code review and we recorded it. We were reviewing server.js file. 
Our work methodology was to go line by line and explain each one.
I learned that others understand different things than I do.(LO1.1.3.1)
I understand now what CORS policies do, because other team members explained this to me.
Policies like CORS allows request from the client.
I think that the code readability is more important than following newest standards
in computer programming, however it would be worth to implement them.
If someone wants to use the code I wrote, should understand its principle.
That is why it is important to focus on readability.
Writing comments above every line is helpful but it is not the best practice in full scale professional project.
It explains the aspects that I might forget in the future.
Fully understanding of every method I use would let me use it in most efficient way.
I should participate more in code reviews.
Experience of the other team members can help me to improve code quality.
I should look for best examples from internet and check how code is written in the books I own. 
I would like to explain difficult topics to others in a better way.
When I hear myself at code review video recording I am noticing things that I should improve.
Like the way I express my thoughts or build a sentence.
I think I should think about how to use the latest JavaScript features and in the same time write the code
easily readable for others. I also think I should go with more initiative and do not hesitate to ask other for code review.

Youtube.com. Live chat team 2022. [online] Available at: <https://www.youtube.com/watch?v=Zwx32nyhHgs&list=PL-LaT8ysl3eE5W8hQJIDVmYL8cmPz9Wip> [Accessed 4 March 2022].