Explain the software development process as aligned to industry practice. LO1.1.2.2
Examine the roles and responsibilities of a typical agile project management team and discuss how they interact. LO1.3.1.1
Explain what is meant by the term “insider”, examining the security implications on an organization’s network, system and data.LO1.3.1.4
Analyse the processes and controls that need to be implemented to maintain the required level of security of a component, product, or system through its lifecycle and at end of life.LO1.3.2.4

I Had a guest lecture with 3 professionals. They have been explaining what does the best developing process 
mean for them and how to apply good practices in their companies.

Barnaby Mercer said that in his company every commit has a pull request. Every pull request has to be accepted at least by
two developers. I think it prevents from writing bad code and protect from lowering security level. Barnaby also said that
crucial pull request related to structure of the whole project or to its security must be accepted by two developers and one leading developer. On my question how are they dealing with security issues, all them replied that they catch every
issue before it become serious problem. I understood that using good control points protects whole project and not give "insider" a chance to harm the application.
Simone and Barnaby agreed that pull requests, containing small changes are the best. I believe it allows to quickly catch the context of the code and review it properly. In the scenario when some code is causing error on live application, they are "rolling" application version back or forward. Which means they are using the last save version of the application or
creating newer version with necessary fixes. I is interesting for me and shows how companies deals with problems.
All above will be worth to implement in to our liveChat team. There are similarities between college project-team and professional one. We are having daily standups where we discuss what we have done and what is our plan for the day.
Our team also does code review, however it is done while whole team is present. It gives the experience of code reviewing for the whole team. 
After a guest Lecture with Barnaby, Maria and Simone. I Learned how to improve code,
software development process and protect it from inside threats.


Insider - "a person with access to exclusive information" (Collins English Dictionary. Copyright © HarperCollins Publishers,2022)

Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/guest_speaker_notes/BarnabyMercer_MariaBel.md> [Accessed 27 February 2022].

