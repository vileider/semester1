Identify and manage deviations from the planned schedule of a project LO1.1.2.1
Explain the software development process as aligned to industry practice. LO1.1.2.2
Examine the purpose, audience and the outcomes to be achieved when communicating with stakeholders, and decide which method of communication to use and the level of formality required. LO1.1.3.3
Describe the software development lifecycles and processes. LO1.2.1.1



I gathered a team to create a project. It has been two and half week of our collaboration.
I found that process I went through, is worth to write reflection about.

I started gathering team of students to create new project.LO1.1.2.2 My main goal was
to find students with lots of will to learn and with positive attitude. I thought it will be hard to encourage someone without
will to engage. From other hand having someone who is moving forward and leaves everyone behind.
I asked 6 students if they want to join my project. Foundations of the Project were two principles:
Starting from basics, because In my opinion quick results will encourage everyone to jump into harder things.
Second principle was strong will to help anyone who need explanation or uplift anyone who struggles for too long.

After our team gathered for the first time I was feeling that I am still missing something.
Tom and Karen as my stake holders/product owners were the best people to ask for advice.LO1.1.3.3
I asked them for small guidance, on what I should focus on and what should I avoid.(Bitbucket, 2022)
Most precious advice was, to ask personally during the meeting, to let team members speak out their thoughts.
I have been told I should avoid leading the team because the team should be self-organizing.
I was looking forward to, use that knowledge on next team meeting. 

First one and half week our team have been doing design thinking until our project has
solidified in a shape.LO1.2.1.1 We were doing this for so long to make every aspect clear for everyone.
Once everyone knew what to do, we populated backlog and set up the tasks.
We were moving slowly but forward. Every time someone has been struggling with a task, other team member was keen to help.
Many times, the team members could not attend for a team meeting.LO1.1.2.1 We use video recording to let everyone watch missing part
and be up to date with the rest of the team. Initial first sprint time has to be one week, however we did not do much during the first week.LO1.1.2.1
Team decided to extend first sprint for another one. We will have enough data for code review or for discussion what goes well and what went bad.

We were struggling to invite our stake holders for daily meeting. I thought that in self organizing team someone should do it.
That was my bad. (Scrum Guide | Scrum Guides, 2022)I was trying to behave as scrum master but I did not remove barriers between stakeholders and Scrum Teams.
It was caused by my lack of knowledge. Finally, one of team member invite personally stake holder and I set up meeting calendar for every day.
Karen was listening carefully our daily meeting. After we finished, she added suggestion that we should speak about our "blockers". We should tell each other
what problems we encounter. I think That will speed up progress in our project. 

When I finished guest Lecture with Bob Willis about agile.(Bitbucket, 2022) I asked him in email, how to work efficiently in a group of students.
He replied that I should find ways to improve my every day - not just at a retrospective. Bob gave me a link to scrum guide website.(Scrum Guide | Scrum Guides, 2022)
I found it as a big source of information about working in Agile. I need to spend more time on that website and put gathered knowledge into practice.

For the Past two weeks I learned that there is always something to improve in team work and it is worth to gather knowledge from proven source.


Scrumguides.org. 2022. Scrum Guide | Scrum Guides. [online] Available at: <https://scrumguides.org/scrum-guide.html#sprint-retrospective> [Accessed 14 February 2022].

Bitbucket.org. 2022. Bitbucket. [online] Available at: <https://bitbucket.org/vileider/semester1/src/main/guest_speaker_notes/Toms_Karen_guidance.md> [Accessed 14 February 2022].

Bitbucket.org. 2022. Bitbucket. [online] Available at: <https://bitbucket.org/vileider/semester1/src/main/guest_speaker_notes/Bob_willis.md> [Accessed 14 February 2022].


