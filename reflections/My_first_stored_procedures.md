
I have never heard about stored procedures. To match Learning outcome I started my research from(LO1.3.2.5)
YouTube videos and tutorial at w3 schools.(MySQL Stored Procedure Parameters, 2022)(SQL Stored Procedures, 2022)

After an hour of learning from videos and articles,(Griffith, 2022) I opened MYSQL workbench
and started my approach to code my first Procedure.
At the beginning I wanted to write something simple like: showing all users from table.
I do not think that writing one line of script in procedure, is the most efficient way of using it. 
I wrote it to learn the syntax of stored procedure before I will create more complicated script.
I wanted to create procedure that would be useful and have some control flow,
so I started reviewing team code. I figured out how to create procedure with parameters,
which will be returning values after adding new records. Procedures are similar to functions and
good practice is to write functions with single responsibility. I skipped that rule for learning 
purposes. After I created that Procedure I could run it on server with just "CALL" invocation and 
name of my function after.

I know how to create procedures and how to use the parameters.(Todorow, 2022, SQL file)
I think procedures are simplifying database query and improves readability.
If queries are repeatable and it is not just a "one-liner" I will be creating procedures.
I learned enough to explain how to write stored procedures and recorded it. (Todorow, 2022 video recording)

In the future I will be definitely using stored procedures in my projects.
In next week I will learn how to use OUT and INOUT parameter.


MySQL Tutorial. 2022. MySQL Stored Procedure Parameters. [online] Available at:
<https://www.mysqltutorial.org/stored-procedures-parameters.aspx> [Accessed 29 March 2022].

W3schools.com. 2022. SQL Stored Procedures. [online] Available at:
<https://www.w3schools.com/sql/sql_stored_procedures.asp> [Accessed 29 March 2022].

Griffith, S., 2022. Learning MySQL - Stored Procedures. [online] Youtube.com. Available at:
<https://www.youtube.com/watch?v=AH0-eAVWpG4> [Accessed 29 March 2022].

Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/video/stored_procedures.txt> [Accessed 29 March 2022].

Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/mysql_code/Procedure_1.sql> [Accessed 29 March 2022].