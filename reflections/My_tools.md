As a person with ambitious to become a professional web developer.
I am using lots of tools, applications or websites and every month I am discovering new ones.
(LO1.2.1.5)
To describe all of them I decided to separate my reflection into 3.

My main code editor is visual studio code.
This application expands to huge working environment when I install extensions.
Extensions at VSC are easy to install and I will explain
the ones that supports me the most at my work as a developer or
as a student.
(Todorow, Web developer tools 2022)
During my studies I work frequently with group of students. **Live share** help us to
write code together at the same file. It speeds up problem solving and
the time to write a component. For example: one person can write imports and other functions.
I am writing styles in SCSS, which can not be read by HTML. **SCSS compiler** converts ".scss" files
into CSS and styles are read properly.
**prettier**, **spell right** and **Vetur** helps me to format document, check spelling and grammar
or keep code syntax on the right track.
I use bitbucket and GitHub to store my repositories. **Jira and Bitbucket Integration** or **GitHub** extensions,
simplify my commits and connections by storing passwords and SSH keys.
Right now I am using web-packs and frameworks, although when I write code in vanilla JavaScript I
use **Live Server**. It simulates server on local environments with live reload.

Extensions marketplace contains enormous amount of addons and I prefer to focus on just a few of them now.
I speed up and simplify my work thanks to them.

My knowledge about already installed extensions could be improved.
At least once a week I will read documentations to reach full potential of already installed ones,
also I will ask friends at college what extensions they use.

Todorow, B., 2022. developer tools. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/developerTools.md> [Accessed 11 April 2022].
