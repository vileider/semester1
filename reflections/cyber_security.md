
Cyber security is my weakest spot. After writing two reflection I still have much to learn.
I will try to describe in couple sentences what I improved and what are my thoughts about it.

When I was creating sensitive data, like passwords and endpoints, I stored it in environment variables
and I passed it to coworkers by mail. Environment variables should not be stored at repository.
Access to database could cause serious risk, when is held by insider.(LO1.1.2.4)
There are other ways of securing database. Using proxy could prevent from overstressing databases with transactions.
(Jackson Higgins, 2022)
I should never sent SQL command from client to server to execute it.
I was predicting that could cause threat, after reading Jackson's article I have noticed I was right. 
(Todorow, Insider 2022)
Authentication for user on different level of access brings another
layer of defense. Captcha - as an automated turning test protect against spam and bots attacks.
By adding password security check I can see second layer.
While I am studying on UHI I have to deal with password that expires every day.
It is annoying but it increase cyber security.

NPM packages could help me to encrypt passwords and sensitive data.
It is better way than hardcoding it by myself however, cloud service provides(LO1.1.1.4 )
even higher level of security. NPM packages can packages carry a vulnerability.(Hámori, 2022)
I have learned that securing whole app from frontend to backend
is not an easy job. I to have plan whole architecture of security.
I think that changes during coding process should be carefully planned.
I should have a big picture of application and modify it with special care.

Lot of my work can be improved. I will gain more experience
by participating in college project, and private one.

Articles and podcast about security are interesting.
I will subscribe at least 3 youTube channels and follow
at least one security related person on twitter.
By this I will keep my knowledge up to date and will learn more.


Todorow, B., 2022. Insider. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/reflections/insider.md> [Accessed 24 April 2022].

Jackson Higgins, K., 2022. Hacker's Choice: Top Six Database Attacks.
[online] Dark Reading. Available at:
<https://www.darkreading.com/risk/hacker-s-choice-top-six-database-attacks> [Accessed 24 April 2022].

Hámori, F., 2022. Controlling the Node.js Security Risk of NPM Dependencies - RisingStack Engineering.
[online] RisingStack Engineering. Available at:
<https://blog.risingstack.com/controlling-node-js-security-risk-npm-dependencies/> [Accessed 24 April 2022].