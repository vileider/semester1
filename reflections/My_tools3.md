
Instead of installing multiple application on my desktop I use websites
which supports me during programming or designing sessions.(LO1.2.1.5)
(Todorow, Web developer tools 2022)
I use **GitHub** and **bitbucket** to store my repositories and 
manage version control. I can see there every participant commit
and I am able to set up branches or add comments after code review.
Every project must have scrum board. I use **Jira Board** for school project.
It is powerful tool and I am lucky to use it for free, however small school project 
do not use its full potential. For private project I use **Trello** In its free version is good enough
to full fill our needs. At my freelance project I work on **Linear**. It is easy in use scrum board which 
helps me to add new task tickets and divide them into smaller ones. In my opinion Linear is more intuitional
than other two.
At every start of new project, I am having designing thinking session. I use **JamBoard** from google.
JamBoard allows Multiple users draw and write their thoughts and ideas in the same time.
Sometimes when I write code I am stuck at some point and I am seeking for solution.
**CodePen** allows to write code that I am struggling with and send the link to anyone.
Thanks to CodePen I could easily pass part of the code and solve the problem.
I get my knowledge from Lecturers, other students but also from online courses.
My main two learning sources are **YouTube**  and  **LinkedIN Learning**.
YouTube gathers anyone who has knowledge and wants to share it for free.
LinkedIn Learning provides professional courses divided into chapters. 
Online tools are giving me flexibility of work. I can be far away from my work station
and still be able to improve my skills or participate in a project.  

I think I could do more professional courses on LinkedIn Learning.
I can earn badges that could be seen by the others.

In next month I will focus more on LinkedIn courses.


Todorow, B., 2022. developer tools. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/developerTools.md> [Accessed 11 April 2022].