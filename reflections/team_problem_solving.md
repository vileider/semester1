
Past three weeks Our team have been dealing with small coding challenges, individual private commitments,
and other difficulties like sickness etc. I decided to write how Our group manage to solve some problems 
and go around the others.(LO1.3.2.3)

Our group is big enough to keep going with the project and meet every day, even if some us can not. Every time someone has to go to work or is not able to attend, they inform the team beforehand. If the task requires presence of current member we reschedule
our daily meeting or change the time when we do pair programming.
We had a problem with development branch. At some point we discovered that we have two. One with
capital letters called "DEV" and another "dev". It was causing push/pull conflicts.
Three of us connect on voice channel and solve the problem by creating pull request to merge DEV and dev.
Pull request expect to be approved by all members. We overcame that problem by changing required participants approval,
down to just three of us. 
Another time, while we tried to implement socketIO library into the project, part of the team(including me),
have been struggling to understand how does it work. I found the solution while one of daily meeting.
I asked if recorded video session, where we would explain, step by step every issue will help.
Everyone agreed and we arrange video meeting in next couple days. I learned the basic of socketIO and 
during the meeting, supported by more experienced member, we explained everything.
I think that the way how we use library and its basic is understandable for everyone now.
In case one of us will forget how to use it there is always recording.

I have learned that good communication and positive attitude is very important.
It helps to solve the problems quickly or at least create solution in the future.

I am not sure what can be improved. I think feedback we share and communication between team members
is on good level. Probably when I will gain more experience I will see it.

It will be worth to watch video or read article about effective technology solutions
and compare it to work flow in our team.


Todorow, B., 2022. Time managment. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/reflections/time_resource.md> [Accessed 19 March 2022].