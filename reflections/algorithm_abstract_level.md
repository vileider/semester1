
There are multiple ways of solving machine problems with algorithms. 
I started reading about algorithms and I tried to practice my knowledge by
coding some of them in python.

(Algorithms and Data Structures Tutorial, 2022)
I wrote couple algorithms in python, Basing on "freecodecamp" course.
(Todorow, Binary search, 2022)
(Todorow, Linear search, 2022)
(Todorow, Linked List, 2022)
(Todorow, recursive binary search, 2022)
(Todorow, tree algorithm, 2022)
Thanks to that I could learn more about data structure
and way of using different types of it.

(LO1.1.2.5)
I have learned what is the structure of array, list and trees algorithms
and how to build them. I understand that some ways of sorting algorithms are more
efficient than other and their space complexity increases with the output.
This relation is represented in Big O notation. (Todorow, algorithm 2022)

I can improve my knowledge of business algorithm usage. By looking for algorithms on google scholar.
It has been to advanced source in my opinion. I understand the basics now and I am happy to dive in to this.


Todorow, B., 2022. algorithm. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/algorithm.md> [Accessed 6 May 2022].

Youtube.com. 2022. Algorithms and Data Structures Tutorial. [online] Available at:
<https://www.youtube.com/watch?v=8hly31xKli0> [Accessed 6 May 2022].

Todorow, B., 2022. Binary search. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/python_coding/binary_search.py> [Accessed 6 May 2022].

Todorow, B., 2022. Linear search. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/python_coding/linear_search.py> [Accessed 6 May 2022].

Todorow, B., 2022. Linked list. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/python_coding/linked_list.py> [Accessed 6 May 2022].

Todorow, B., 2022. recursive binary search. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/python_coding/recursive_binary_search.py> [Accessed 6 May 2022].

Todorow, B., 2022. tree algorithm. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/python_coding/tree.py> [Accessed 6 May 2022].