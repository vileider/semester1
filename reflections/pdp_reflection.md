

Second semester comes to the end, so I decided to review my personal development plan.
(LO1.2.1.3), (Todorow, 2022, PDP)
Starting from the top of document. I have noticed, I would be able to match my skills to
requirements at job adverts pasted by me. The only issue I have, is with my portfolio. I do not have one.
When I review **my vision for myself for the future** I can see what skills I improved
and which still needs to be polished. My JavaScript Skills are much better. I worked a lot
with objects and arrays. I have been practicing function and variable scope. I spend much time
with React Typescript and my knowledge about adding types and interfaces is better. For the past
couple months I worked with VUE Framework. I understand how components are built and basic usage
of VUEX store.
Creating Rest API on Node.js for the school project and server less one for my freelance job 
showed me the differences and similarities between these two solutions.
I worked in Agile way at my private project, freelance and school one. I read articles
and videos about agile good practices. 
When I look at my strengths and weaknesses I think I have changed my stubborn into strong
will to achieve goals but not at all costs. I was easily irritated by silly things.
I looked for help in this matter. When I was writing this personal development plan
I did not know much about databases.

Right now I can recognize PostgresQL, DB2 and MYSQL. I can design architecture of database
and manipulate database structure and data. 
I understand how to start a project and move on with daily
meetings. I rationalize why I want to achieve current goal and I seek for most effective way.
I think I still care about project related stuff a lot, but I gained more distance to everything.
I learned more than I was expecting from myself. 

I should create proper portfolio basing on other students experience and update my CV.
I would like to Improve even more in VUE and REACT. Cloud services is what I would like to
dive into. I think I should improve time management. I am still easily distracted.

First what I should do in the last month of second semester is to start focusing on one goal.
Accomplish one after another. Thanks to that I will save more time for skill improvement.

Todorow, B., 2022. Personal development plan. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/larger_text_files/PDP_Bogdan_Todorow.docx> [Accessed 18 April 2022].