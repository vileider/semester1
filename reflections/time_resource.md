
Our team had ups and downs while developing the project, caused by each student personal commitments.
Ups when most of us had time to work under live chat. Downs when non of us had time to push more code to repository.
We have been treating sprint time and end of 2nd semester as a contractual obligation.
We tried to balance between evidence gathering and upgrading existing application.

Sometimes when we did not have enough time we implemented basic functionality of new feature.(LO1.3.2.1)
It did work properly when we did not change anything in other components or on server.
It looks like, our code have not been extensible.(Todorow, B., 2022).
When we have been lucky to have everyone on board, we did code reviews. 
During code reviews we have been doing small refactoring and clearing code.
(Live-chat Team, 2022, server review), (Live-chat Team, 2022, VUE file review)
Also we have been adding more comments to the code - which as we understood later,
did not help in code readability.

I have learned that sometimes I have to extend estimated program finishing date.
Quality is important. By writing clean code(extensible, readable), team save time in the future(Todorow, B., 2022).

Even when we have each member time schedule,(Live-chat Team, 2022, members schedule) it is hard to move forward faster.
We should discuss that on our daily meeting, how can we resolve that.
Also we should consider writing better quality code over quick implementation.
Another thing in my mind - writing comments. We should rethink the reason why we put them 
into the code. (but that is a subject for another reflection)

I think the best idea is to look for articles, videos about how write good code, and to finish
contractual obligations in time. Discuss that subject with friends who work for as a developer.
Gather all information and discuss with the Live-chat team how can we improve our weak points.


sprint time -contractual obligation 
absence, knowledge of each members -resource constrains
quality over time

Live-chat Team, 2022. Server code review. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/video/server_code_review.txt> [Accessed 19 March 2022].

Live-chat Team, 2022. VUE file code review. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/video/vue_message_display.txt> [Accessed 19 March 2022].

Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/guest_speaker_notes/Tom_refactor.md> [Accessed 19 March 2022].

Live-chat Team. 2022. team members schedule. [online] Available at:
<https://bitbucket.org/JuliaKloda/live_chat/src/master/time_management.txt> [Accessed 19 March 2022].