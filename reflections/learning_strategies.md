
(Todorow, Soft and technical skills., 2022)
Before I started this reflection I clarified what soft and hard(technical skill) is.
I will write what I did to created my own learning strategy and how did it go.

I was looking back at my code from previous projects for comparison.
I have noticed that I was able to implement more advanced techniques like
advanced interfaces, more readable code. VUE as my second JavasScript framework was
enigmatic form me before. Right now I can play with the component state and lift the props.

(Todorow and Basiul, 2022)
I started third version of my private project which has modern UI. Redesigned catalogue structure
and new concept of reusable styles and components. We also put attention on documentation which 
is going to be build up parallelly to the rest of the project.
(React 18 - What's New, 2022),(San Martin Morote, 2022)
I keep up with VUE state management or React Updates, by searching internet for news.
(LO1.3.1.3)
My learning strategy is to dig into the subject which looks interesting to me and 
give me a joy of learning new things. By keeping myself entertained while learning new things
I am not burning out.

(Todorow, PDP review, 2022)
I looked at my Personal development review and I can notice that my plan was 
almost 100% successful. Most important thing I learned and I am still learning is 
to how write reflections, notes and logs. It is not easy but if I will pass 1 year.
I will know better how to organize my work. I learned that during second semester 
I could match many guest lectures or events to my Learning Outcome evidences.

I was hesitating too much with writing about my progress. It could be caused by my
private life issues, but I know that voice notes exists and I should not have excuse
for lack of evidence. Help of Karen and Tom should be considered more often.

With the knowledge I already have and many sources of information. I will be able,
to keep improving my soft and technical skills.


Todorow, B., 2022. PDP review. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/reflections/pdp_reflection.md> [Accessed 6 May 2022].

Todorow, B. and Basiul, S., 2022. bosoreactproject3. [online] GitHub. Available at: <https://github.com/szymonbasiul/bosoreactproject3.git> [Accessed 7 May 2022].

Youtube.com. 2022. React 18 - What's New. [online] Available at: <https://www.youtube.com/watch?v=N0DhCV_-Qbg&t=337s> [Accessed 7 May 2022].

San Martin Morote, E., 2022. Pinia 🍍. [online] Pinia.vuejs.org. Available at: <https://pinia.vuejs.org/introduction.html> [Accessed 7 May 2022].

Todorow, B., 2022. Soft and technical skills. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/Soft_and_technical_skills.md> [Accessed 7 May 2022].