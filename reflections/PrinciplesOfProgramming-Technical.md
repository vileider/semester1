LO1.1.1.5Technical
Write programs in a variety of languages that can use common features for control flow and data structures.

Before writing this reflection, I was thinking what would match learning Outcome 1.1.1.5. I try to solve the same logical problem, in two not so different programming languages: python and JavaScript. I would be worth to compare more that these two in the future.
I chose Python as second programming language because as its popularity and I am curious of its abilities to create Graphic user interface.
To start comparison, I finished 2 hackerrank challenges in python and I accomplished them in JavaScript. Python seemed to be easier to working with data when I compare it do JavaScript. When I look at task where I had to comprehend the python's lists. (Todorow, 2021). Python looks more readable, and code is shorter. I have been playing with JavaScript for more than two years although I work with python couple months, and it looks simpler to me. (Todorow, 2021). JavaScript solution does not look bad. It is noticeable that code is missing that easy level of understanding. I had to look at it for a longer moment. It is embarrassing I had to use 6 different JavaScript methods to achieve the same goal. Perhaps after couple years I will be able to write it in more elegant way, still Python is winning if it is about working on arrays. Is not that I am only fascinated, by the new learned language. There are cons Of Python like being restrict if it is about tabs and spaces. Maybe that what its readability coming from?! JavaScript is more tolerant if it is about code formatting, although I am forced to use "prettier" or "eslint” for coding comfort. No curly braces at Python looks little bit odd, but I get used to it after some time.
I cannot wait to test other JavaScript and Python capabilities, like Async and server deployment.

#

Todorow, B., 2021. Task2. [online] bitbucke.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/python_coding/pythonTask2.py> [Accessed 12 December 2021].

Todorow, B., 2021. equivalentOfPythonTask2. [online] bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/javascript_coding/equivalentOfPythonTask2.js> [Accessed 12 December 2021].
