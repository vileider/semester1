
While working under my freelance project I was lucky enough to create
relational database from the beginning. I will try to describe that process

Before I will do that I should write that it is not my first database.
I created first useful in workflow, for my private project.
Another one was made in DB2 for habits help with a huge help of students.(Habits Help Team, 2022)
Contained just the initial tables: users, habits and successes's.(LO1.3.1.5)
Thanks to this project I understood how to add keys and define constrains.
I understood also what is the relation between primary key and foreign key.
Next database I created with live-chat team, taught me how to create Stored procedures.(Todorow, 2022)
After that experience I was ready to create more professional database.

I was creating relational database based on stakeholder instructions and known basics.
(Todorow, 2022,database basics)
I can not give an evidence of that because it is confidential.
At the beginning I had a stand up and team were discussing database purpose.
After the meeting I read all documentation. I wanted understand how does database will
relate to the whole project. For the second meeting with stake holder I prepare database frame
and concept of relations. I created first couple stored procedures.
We discussed my plan and adjusted where it was needed.  

Constant communication is essential when creating fundaments of database.
Decision where to put functionality should be made at the beginning.
Logic of app could be balanced between database, server and front end.
I learned that project specification determines which part should be stressed the most.

My knowledge about database could be improved. I will watch SQL course at 
LinkedIn learning, also I will show my SQL code to one of the students. 
I am hoping to receive insightful feedback.

Habits Help Team, 2022. Initial DB2. [online] Available at:
<https://bitbucket.org/vileider/semester1/src/main/mysql_code/DB2_habitsHelp.sql> [Accessed 27 April 2022].

Todorow, B., 2022. stored procedure. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/reflections/My_first_stored_procedures.md> [Accessed 27 April 2022].

Todorow, B., 2022. relational database basics. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/other_notes/relational_database_basic.md> [Accessed 27 April 2022].