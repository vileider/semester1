Show how a business could be vulnerable to threats from systems and people. LO1.3.1.2


I have been dealing with databases in the past but guest Lecture with Florin Anton
gave me wider view over this subject.
I have been rewatching video recording With Mr. Anton about 8 times. The subject was difficult to me,
and I wanted have deep understanding of that topic. 
Big companies databases might be vulnerable due to high volume of users retrieved data might
be incomplete or contain fake records(phantom reads). In other case, users can block each other while trying pick
some information from database(deadlock).
Florin explained (Todorow 2022) that Isolation level is the solutions for most of databases problems cause by high traffic.
Based on DB2 database explanation Showed me that 4 isolation level can prevent from loosing data 
or situation when Non-repeatable read occurs. 
Higher isolation level like Repeatable read, protect the database the most. It locks whole table and only one transaction has right to access to a table. Lower isolation level increase the volume of transaction however it leads to possible 
phantom reads or Non-repeatable reads. I think it is about compromise and good sense which table need specific level of isolation. I do not have enough experience in database but this guest lecture inspired me to think about it and use in future projects.
 
 Todorow, B., 2022. Bitbucket. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/guest_speaker_notes/Florin_Anton.md> [Accessed 22 February 2022].