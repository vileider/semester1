
Managing time is my biggest weakness.
I will write down how I tried to put in place my life work balance.

(Todorow, Time resource, 2022)(LO1.3.2.2)
I have been working with the team and we were able to build up our application.
Our project progress was going well until everyone realized we need to focus on evidence gathering.
Even if my college team could not focus on the project I was still working on my private one and 
the freelance job.
The application coded in react with my friend was still moving forward. We knew that we if we will 
aim too high we can burn out. We had to adjust the scheduled meeting to avoid collision with our personal commitments.
We worked three times in a week, maximum 4 hours.
The freelance job was just a couple hours a week, however I was hardly able to squeeze extra working hours in the week. 

I think everyone in live chat team had the same problem. We did not write the evidence of
our progress. We stopped our daily's and focused on learning outcomes which as I understand
now, is not the way we should study. 

Next year I will explain to myself what evidence I need to gather and then
I will write it as soon as I will learn something new.
I feel that I am easily distracted. That is the reason of my chaotic way of learning.
(Todorow, managing time note, 2022)
I feel I am conscientious but it is hard for me to focus on one thing.
It is something I should work on.



Todorow, B., 2022. Time resource. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/reflections/time_resource.md> [Accessed 9 May 2022].

Todorow, B., 2022. managing time note. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/managing_time_note.md> [Accessed 9 May 2022].