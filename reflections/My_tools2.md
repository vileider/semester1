In my opinion, modern web developer must know and understand couple cloud services.
As a student at UHi I am lucky to work and discover IBM cloud for free.
For contrast and comparison I also use AWS cloud services, which is not free but still
cheap enough at "free tier".
(LO1.2.1.5)(Todorow, Web developer tools 2022)
IBM toolchain helps to automate project deploying process.
Instead of manually publish and build the project I can set up
all of that to be done by cloud service. It speeds up whole publishing process, giving more time
to focus on clean code.
Database at AWS and IBM lets me to store data and easily connect it to other Cloud functions.
I use node express server hosting at IBM. Server written in JavaScript gives me comfort of writing
frontend and backend in the same programming language. I am happy that such a service is available.

I started using AWS server less cloud function. Amplify CLI and Lambda functions are still
new to me, however I am learning and discovering it every day.

Cloud services are powerful and expensive in the same time.
I it is worth to spend time and work in their environment. I am finding lot of
job advert where employers require cloud service experience

I think my understanding of AWS could be improved. In next couple days I will read
Lambda documentation.

Todorow, B., 2022. developer tools. [online] Bitbucket.org. Available at:
<https://bitbucket.org/vileider/semester1/src/main/other_notes/developerTools.md> [Accessed 11 April 2022].
