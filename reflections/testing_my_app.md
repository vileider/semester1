"Once my app is stable enough for testing, I should write tests".(Helppi, 2022)
I have been testing UI and one component function.

I tested my app using TestCafe and Jest.(LO1.1.3.5)
I wanted to write a test that checks if dark mode button works.
Button has simple functionality. It was the best way to start
Learning how to write tests. Me and other student wrote a function that checks,
if background color changes on click.(Todorow and Kloda, 2022)
Our next task was to test reusable component.(Todorow and Kloda, 2022)
We used Jest JavaScript testing framework. We wanted to check if function
returns right value. Both attempts were successful. 

Testing helps to check stability of application after I added new functionality
or did code refactoring. It takes some time to write it, but it saves much more time
in the future. I do not need to test every function and every UI component manually.
Whenever I do changes in the code I run test to make sure my app is working as it should.
Testing Frameworks have similar syntax. Once I learned how to write tests in Testcafe,
writing another one in Jest was easy.

I need to practice. Tests should be written before functionality, however if I will
not know test syntax well I will not be able to do it.

I will implement test to my private project every time I will add new funcitonality.


Todorow, B. and Kloda, J., 2022. dark mode testing. [online] Bitbucket.org. Available at: <https://bitbucket.org/JuliaKloda/live_chat/commits/833889842fcac8fc5f31d95a18368fb55eec0b66> [Accessed 20 April 2022].

Helppi, V., 2022. Test Early, Test Often, Testing as Part of App Development - Bitbar. [online] Bitbar. Available at: <https://bitbar.com/blog/test-early-test-often-testing-as-part-of-your-app-development/> [Accessed 20 April 2022].

Todorow, B. and Kloda, J., 2022. testing function. [online] Bitbucket.org. Available at: <https://bitbucket.org/JuliaKloda/live_chat/src/a1bb0816bff54e5b0acfa3351c3af66506709747/react-chat-app/src/components/button/__test__/button.test.js> [Accessed 20 April 2022].