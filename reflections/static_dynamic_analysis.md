
I started researching subject related to code analysis.
From first look, Software analysis is detecting potential threats.
I will write what frameworks I found and to what software analysis can be used for. 
(LO1.1.3.4)
I found many frameworks that helps with SCA and DCA, however I will focus on free ones. For the coding amateur like me they should be good enough.
* Snyk is made to check security and quality in JavaScript, Java, Python
* sonarqube is designed for 29 programming languages. Checks Security, and Maintainability. Most important form me: JavaScript, python, HTML5, C sharp, C++.
* Codacy monitors: code style, best practices and security,
designed for Sass, SQL, typescript and many more
Every sources says that best practices are: debug as you develop.
Ca frameworks are designed to check quality issues and provides advices to help me quickly fix and move on with the code.

Analysis can be divided to static and dynamic one.
(Static code analysis, 2022)
Static happened when code have not been deployed yet.
I can start as soon I have a piece of code to prevent costly defects.
It prevents from late refactoring frustration.
(Dynamic code analysis, 2022)
Dynamic analysis is, only possible when application is running(on the production). It can reveal more application exploits.

I think my next reflection will be describing the process
of code analysis framework. It will be a good combination of code review and code analyzing.

I am not going to lie myself that I will use code analyzing
tools every day. Although I should start a habit of using it 
approximately once a week. I should consider, to use it even more
often when I will become more confident developer. 




Youtube.com. 2022. Static code analysis. [online] Available at: <https://www.youtube.com/watch?v=xQKCpJnmLNM> [Accessed 21 April 2022].

Youtube.com. 2022. Dynamic code analysis. [online] Available at: <https://www.youtube.com/watch?v=6okVFkDKORg&t=149s> [Accessed 21 April 2022].