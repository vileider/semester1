
"You can’t manage what you don’t measure. Accordingly, you can’t monetize what you don’t manage."
-unknown Author.(Laney, 2022)

At Stedman article I read that Maximizing company potential should be done with gathered data.(Stedman, 2022)
If the company analyzed everything in a graph, conclusion could lead
to increase profit, decrease costs or finding new target group.
(LO1.2.1.2)
Every company entities must stick to the rule of 2018 Data Protection Act.(Data protection, 2018)
Fragile Data should be used with the respect to data sensitivity.
Age or gender can be used to personalize adverts. 
To authenticate me as a user government application or services like PayPal require photo of
ID.
According to Cezarina's Dinu article, integrity can be obtained with multifactor authentication. (DINU, 2022)

I Understand now that data Companies must keep it up to date and kept it for no longer than is necessary.
Data can be the main factor while organizing work.
While reading data Protection Act 2018 I Learned that full rights are bound to data subject.
No I know that If my confidential data would be stored somewhere I can get all information that is stored.
I can even ask to delete it.

My whole knowledge about treating information has been expanded thanks to articles I read.
While I work in a group of students or do my freelance job I should keep in mind what I just
Learned today.

I think watching full documentary movie this week, related to value of stored data could be
a good Idea.


Stedman, J., 2022. The Ultimate Guide to Big Data for Businesses. [online] SearchDataManagement. Available at:
<https://www.techtarget.com/searchdatamanagement/The-ultimate-guide-to-big-data-for-businesses> [Accessed 19 April 2022].

Laney, D., 2022. Your Company’s Data May Be Worth More Than Your Company. [online] Forbes. Available at: <https://www.forbes.com/sites/douglaslaney/2020/07/22/your-companys-data-may-be-worth-more-than-your-company/> [Accessed 19 April 2022].

GOV.UK. 2018. Data protection. [online] Available at: <https://www.gov.uk/data-protection> [Accessed 19 April 2022].

DINU, C., 2022. Data Integrity: What It Means and How to Maintain It. [online] Heimdal Security Blog. Available at:
<https://heimdalsecurity.com/blog/data-integrity/> [Accessed 19 April 2022].