Thursday 23.9.2021
I will Learn the structure of catalogues in Vue

### Friday - sunday 24 -26.9.2021

I download couple times VUE - discovering differences between options at vue installer.
Watched couple VUE tutorials and videos about differences between VUE, REACT and ANGULAR.

### Monday 27.9.2021

I Learned how to work with jira and I improved
my interpesonal skills on 3 team meetings.

### Tuesday 28.9.2021

Learned differend ways of styling divs.
Added branch to team project.
Worked in pair on the code

### Wednesday 29.9.2021

I refreshed html/css drop down menu element.
I practised working on jira agile gui and pushing, pulling and merging branches on bitbucked.

### Thursday 30.9.2021

Have been learning vue at linkedin course
I carefully watched 1st and 2nd chapter

### Friday - sunday 1 - 3.10.2021

I was still watching vue tutorials
on youtube and freecodecamp, and I was reading
documentation at https://vuejs.org

### Monday 4.10.2021

I was programing in pair and I managed to
put all project participants together, by creating
seperate components on our website.
I inject other students code and modified little bit
to make it work. I was participating in one of the
students video semi-tutorial how to connect and configure server, database on IBM cloud.

### Tuesday 5.10.2021

I have been helping other students to set up programming
environment oth their machines. I recorded code review with 2 other students. https://uhi.webex.com/uhi/ldr.php?RCID=a8a32ebf6fe48e49954d3698d3d02392 CODE: vNfAtt3h
We(students from team blue) arranged conversation about tasks and we rearranged sprints.

### Wednesday 6.10.2021

Atend to mentor progam session

### thursday 7.10.2021

Watched Vue Tutorial on linkinlearning
finishing blue team project

### friday 8.10.2021

finishing blue team project

### holyday

### monday 18.10.2021

attend to Tom's meeting
https://uhi.webex.com/uhi/ldr.php?RCID=3f6138d2b9cf2249100ed11237828845
pass: xUb3efSC
I Registered at https://www.hackerrank.com
I started "zxplore"

### tuesday 19.10.2021

I tried to understand how does the portfolio
creation on portal works.
attend to Tom's meeting about datatypes
https://uhi.webex.com/uhi/ldr.php?RCID=8acc23d600e16327c5f73d171d0ead4e
pass: rTMNYBU6
I arranged meeting with two other
course members to chat about portfolio.
I Installed python on my deesktop and
started tutorial on linkedlearning
Programming in a pair on react project

### wednesday 20.10.2021

I attend to guest lecture with John Dever
I started python preparation
and achieved bronze start
[here](./images/bronze_medal_python.png)

### thursday 21.10.2021

I have been learning python at
linkin learning.
I arrange meeting with my mentor for monday.

### friday 22.10.2021

I have been learning python at linkin_learning
I manage to finish my degree submission process.

### monday 25.10.2021

I had a meeting with my mentor.
I had team habits meeting.
I have been programming in pair with my friend.
I watched another chapter of python tutorial.

### tuesday 26.10.2021

I have been at daily team meeting
After meeting we have been miro dashboard
to visualize first stage of the Design Thinking.
writing every idea
I have been programimming in pair, working
under my private project

### wednesday 27.10.2021

We had daily team meeting in the morning
sharing concepts with each other.
After the daily meeting we populate the jira board
at 1 o'clock I attend to guest lecture with
Mr Armen.
I have been learning about react state managment and
js "..."(3 dots) operator.
I watched another chapter of python tutorial

### thursday 28.10 2021 - sunday 31.10.2021

Until friday I was working on a project with
the team-good-habits. We have been setting up daily tasks. I was trying to understand IBM cloud.
I recorded video in which I explain how to
create server and database on IBM cloud.
How to connect server to database and set everything up.
I also work on my two other project with two differend friends

### Monday 1.11.2021

After daily meeting I tryied to work on server database
communitcation. Errors and "connection" problem stopped me from getting involved into project on monday.
I watched another chapter of python tutorial at linkin learning. Afternoon I worked on my private projecyt. I was programming in pairs with my friend.

### tuesday 2.11.2021

After daily meeting I was learning IBM by helping other students who got stuck into the IMB cloud connection problem. Afternoon I worked on my own project.

### wednesday 3.11.2021

Wednesday morning meeting asigned me to work on server to database request. I tryied to work on IBM cloud however, I just wasted 3 hours bumping out of it's errors. At 1 I attend to guest lecture in which Douglas explained principals of copyrights. I finished Designing RESTful APIs course [here](./images/Designig_REST_API.png)

### Thursday 4.11.2021

At morning meeting we described daily task.
I finished reflection based on complited Restful API course. [here](./larger_text_files/REST_reflection)
I watched All Hands Course Feedback Meeting Recording.
I started voice meeting with team member and discussed
issues related to IBM cloud.
I send late personal development plan
[here](./larger_text_files/PDP_Template.docx)

### Friday 5.11.2021

I consulted my ideas with other team members. and later on I was reading about databases and its commands.
I watched couple tutorials and I have been practicing database commands.

### Monday 8.10.2021

After morning meeting I watched tutorial and read documentation. Also I checked my previous projects.
I add endpoint that let pull specific data from database and I clean little bit server file.
afternoon I add test to my project.

### Tuesday 9.11.2021

very long meeting was until 2 pm. I remided myself how to
set up routes on the server and I refactored
server file little bit. Afternoon I watched python tutorial and after that worked on my project by adding tests and created functionality to match the tests.

### Wednesday 10.11.2021

On daily meeting everyone disscused plan for the following days. I created script for db2 database.
at 2 o'clock I had guest lecture with security specialst.

### thursday 11.11.2021

I did not do much.
after I sort out private tasks I trace my team on
slack. I watched another chapter of python tutorial

### Friday 12.11.2021

After morning meeting with team I attend to Douglas session. I have learned how to use figma more effectively.
I watched one chapter of sql tutorial.

### Monday 15.11.2021

I instaled win 10 and all programming enviorment.

### Tuesday 16.11.2021

I finalize win 10 instalation and started reading IBM documentation about database.

### Wednesday 17.11.2021

After morining meeting I managed to get data from my local server and database.

### thursday 18.11.2021

I attend to quest lecture meeting with Steve Penson.
I add getting data from database functionality. I manage to send data to database on my local server.

### friday 19.11.2021

After daily I worked on sending data to one of the
endpoints on IBM server.I managed that at evening time.

### saturday - sunday 20 - 21.11.2021

I finished Node.js: Securing RESTful APIs course
[here](./certificates/Securing_REST.pdf)
I finished Securing Frameworks course
[here](./certificates/Security_Frameworks.pdf)
I finished Node.js: Security course
[here](./certificates/Node.js_Security.pdf)

### monday 22.11.2021

I had to rewrite credentials in to the team ones, because we were using created ones just for test.
I create add habit endpoint

### tuesday 23.11.2021

I add deleting habits endpoint

### wednesday 24.11.2021

I Have been leading code reveiw based on node-express
api I created
I wrote reflection about security
[here](./larger_text_files/security_reflection.md)

### Thursday 25.11.2021

I had a conversation with Tom about learning outcomes
I was working on React project and I was trying to find a way to pass the variable into function at
called component. I figured out the way by executing
the props as a function inside the functional component with variable as an attribute.
I was adding another endpoint nested inside its parent with dynamic parameter. I tried differed ways of doing that and finally it worked. Unfortunately requested body has been returning undefined value.
The value which supposed to be posted body I set into default as 1 and finally. Thank to that I received expected data from database
I learned how to create endpoint with dynamic parameter
I should find the way to pass requested body to a
value
I need to look for a tutorial and documentation which will show me how to pass request body to a value without a problem

### Friday 25.11.2021

I was working on a private project and I tried to learn typescript interface. After half an hour of looking for solution I ask on slack. Douglas helped me to understand that the task I was struggling with, was complicated by sophisticated variable name founded by me at some website. I figured out that I could name the variable anyway I wanted. That solved my problem and in future I Will look more carefully at examples on internet.
start discussion about reflective writing and time management with Karen. She advised me basing on her example to create habit tracker, use calendar with reminder. It was very usefully for her in a past.
I pass two task at hakkerank supported by linking academy. When I was struggling with the task I took a small break. After coming back to the task I used advices included in LinkedIn learning tutorials. with the fresh mind problems becomes easy to resolve.
I watched webex recording about evidence gathering
and about learning outcomes. every time I re watched it again, I am learning new stuff and understanding more topics.

### Monday 29.11.2021

after meeting in the morning, I checked my email. Found an invitation from hackerrank. I took up the challenge. my task was to display number starting from one until the number which was provided by using the loop.
For couple minutes I was trying to solve the problem by using already known coding patterns like: when I create function I have to named it and define, if it returns something or has some logic inside, or when I use conditions I need to check if that block of code is true. Unfortunately I could not turn integer to string variable. I googled "int to string python" and I entered in to https://careerkarma.com/blog/python-string-to-int/ website. I have find out the way to solve the problem and I get bronze badge as award. I was thinking about Karen suggestion - creating habit tracker. I created one with couple tasks on paper and I sticked it to the fridge. I have been pair programming with my friend. We get to problem which seemed to be dead end. We could not give types for an object that we created. We tried to look similar examples on google. We tried to erase variables and step by step write types for each variable. Finally, we decide to swap our places. He become navigator and I started to be a driver. I refactored whole code and at the and I looked at the task that have been bothering us for more than hour. I found the solution - which was to many brackets, unnecessary variables or unassigned variables. https://github.com/szymonbasiul/bosoreact2/commit/b18ab200b4d9de5e1ec93ca046cdf1a88b2c3640
Next time when I will stuck, I will try to clean up my code and look at it from different perspective. I watched Uncle bob Agile video:
https://www.youtube.com/watch?v=FedQ2NlgxMI&t=3249s.I haven't come to any concrete conclusions yet. so I will watch it again.

### Tuesday 30.1.2021

Today my team requested for two more endpoints. One for editing existing habits table and other for receiving all items from success table.
I never created endpoint for editing, although I remember CRUD concept. Which means: create,read,update and delete. I typed CRUD in google and I found: https://www.bmc.com/blogs/rest-vs-crud-whats-the-difference/.next. I have learned that endpoint will have method 'PUT'. I had to use my favorite teacher at YouTube: "traversymedia":
https://www.youtube.com/watch?v=L72fhGm1tfE&t=2270s
with his help I managed to create both requested endpoints:
https://bitbucket.org/JoryFrench/habits-help-2021/commits/f22d530ebc661ed4b6cf233ba166144a2e99cc20
&
https://bitbucket.org/JoryFrench/habits-help-2021/commits/0def092721faf9bdeaec60b86369488539cec77d
After finishing daily task for team habits I started working in pair programming with my friend.
He showed me what he learn at his college course.
We started using Pseudo-classes and Pseudo-elements to improve scss's visual effects. We remember previous experience, do after writing some code we cleaned and refactored it straight after.https://github.com/szymonbasiul/bosoreact2/commit/3e3bfc97de40f80544b13b3e2970e0316e889a54 At the end we use our scrum board to prepare task for next programming session.

### Wednesday 1.12.2021

After a morning session with my team I continued work order REST api. I gather request from team habits members and I started improving server files
https://bitbucket.org/JoryFrench/habits-help-2021/commits/e25dd2f2b4c8851e3d322138b61bbe9da095a0bf
I have been noticed that there is small inconsistency in variable names so I search every endpoint to unify it. I should be more careful while I add another endpoints to server. I should studies the code first and while I am writing I must make sure every endpoint gathers and send data in the same format.
That stops unnecessary confusion at frontend developing.
Afternoon I attend to guest lecture with Simon Tasker
I wrote notes that can be find
[here:](./guest_speaker_notes/simonTaskerBoeing.md)
Todorow, B (1.12.2021) 'simonTaskerBoeing.md' [1/12/2021]
Around evening I went to a Douglas session about native apps. I Learned that for testing I can use library which lets me scan QR code and display written app on my phone. I can use "Device farm" on AWS which is relatively cheap. I should also consider on what platform I would like to write an app. Google store is cheap however chances of possible earnings are low.
IOS at AppStore is restrict and expensive but application has got high chance for profit.

### Thursday 2.12.2021

At the beginning of the day I tried to add another endpoint that would respond with every success that is bound to habit. https://bitbucket.org/JoryFrench/habits-help-2021/commits/45fd184244ed57177157136ec1450b64c6502aca
When I was in the middle of my work. One of team member start a conversation about our project, and I understood that this type of endpoint is not needed. I started a voice chat to precise what kind of endpoint we need. I realized that often chat between team members is essential. It saves time that is wasted by unnecessary work. Afternoon our team had a meeting. We discussed how can we prepare for our presentation. We decided that each one of us will show one's work and at the end and we will finish with the final conclusion.After meeting, while I was creating endpoint for success deletion,
database has been erased and recreate. It was impossible to create any habits or success without created user, because of foreign key at habit table which was the primary key at user table. User has not been created and the person responsible for database was not available that day anymore. I think that such a big affair like database recreation should not be done at the evening time. In case of unexpected issues, whole team have to wait until next day to continue work.
I had an endpoint for adding user, however I had to add user password. the password has varbinary type. I have never seen this type before. No one in my team admitted to set this variable. There was no point to argue. I start googling varbinary types and how to create it. After half an hour of digging into db2 documentation and I have found database function to create it.
https://www.ibm.com/docs/en/db2-big-sql/5.0.2?topic=functions-varbinary#r0061490
I could finally finish add user endpoint and repopulate database. I created functionality on test site as well.https://bitbucket.org/JoryFrench/habits-help-2021/commits/0859f0e1b06cccbb3b4248f54e05ac685351aa95

### Friday 3.12.2021

Today I had a webex meeting with Douglas and other students. It was explained how can we gather resources for our reflections. One of important part of creating reflections, logs or daily logs was to be critical about resources -
like guest lecturers, conference talks.
If resource does not look trusty I should compare it to multiple others. Douglas explained that creating lots of work is important. That will let me pick most relevant reflection,piece of code or recording.
We have been warned that referencing to notes about not anymore existing videos is a bad idea because it would be nearly impossible to validate that and value of evidence this type would drop significantly.
I have been shown Bloom's Taxonomy as a hierarchy of cognitive understanding.
From my understanding at 1 semester we should polish first and second level of taxonomy - remembering and understanding.
I worked later on a team project- I had to amend multiple records in one query. I never did that before. I google it and I have found satisfying answer at: https://stackoverflow.com/questions/20255138/sql-update-multiple-records-in-one-query
I took a hakkerrank challenge about writing a program in python with use of if statement.
I had to check if provided year is a leap year or not. I have been struggling with it and even asked on slack channel for small help. Douglas and Jordan helped me with variable assignment. after 3 attempts I finally wrote the right answer. Record of my development can be find [here:](./python_coding/gregorian_calendar.py)

### Monday 6.12.2021

I attended to Douglas meeting about simplifying code and keep readability. I have been introduced to Karnaugh maps- that was something new for me I did not understood it 100% I need to read more about. Tom mentioned about interviewing hypocrisy, and I remind myself about interviewing video I watched yesterday - which rise the same topic. https://www.youtube.com/watch?v=JEyX6GD0SX0 . Tom says that interviews can show a lot about company that you might will work in. I did not have any programming interview experience in my life, however I will keep that in mind
Later on, we discussed different approach to the same task. I have noticed that writing code in one line will not let me debug code in effective way. Lots of function will let me test it in more effective way. I should run "profiler" before optimizing. When I write code, debugging should be my priority. After my code is bug-free, I need to think about optimization. Most of the time I have been guessing what is the best way of programming.
It is better if documentation is written in separate file. Douglas mention about "Oxigen" program that helps to write documentation- I need to take a closer look at it. He said also that comment should say why the code is there and what it does, and I shall not let the comment be out of dated when I change the coding line. If I write description comment in program file I should do it at the top.
I will try to implement and practice everything what I learned during todays morning session.
Today was the day of our team project presentation. Every team showed 2 months team work. I had a feeling that my team did the biggest project. Ironically we were stressed because we thought we not going to handle this. Tom, Douglas and Karen have been criticizing everyone's work in a constructive way. They prove that structure of catalogues should be more intuitive. On next project we should use different repositories for applications, frontends, We have been told that our entity diagram should be little bit more specified. It was a good experience, and it was nice to see differed approach to the same task.

At the evening time I was writing program in pair programing. We implemented commenting method described by Douglas at todays session. we find it very useful because there will be no more misunderstanding caused by unparalleled ideas for function components. Later I spoke to teamhabits team and we talked about server. We come to a conclusion that it should be refreshed only by certain repository update and that would force us to split repositories into couple smaller ones.

### Tuesday 7.12.2021

Today I was working on Agile reflection for the most of the day. I have been doing small breaks from it and I watched Tom's Short video about referencing: Short Videos - Finding academic papers and referencing resources
I fixed grammar mistakes starting from Thursday 25.11.2021. I left daily log before that date uncorrected, because I should to see the progress - as I was advised by Tom
I helped Slav to convert class component into functional one in react native.I can say that as a return he showed me how to use live Share - visual studio code extension.
After an hour long session with Slav I use that extension in a pair programming with my friend later. We speed up our progress for 30%. We could show to each other what line of code does what, while we have been talking to each other.

### Wednesday 8.12.2021

I added small functionality at team habit server. It was Andrew request to get response with success Id once we add one
I was looking for information related to lo1.1.1.3 outcome.
I found couple articles about but I included the most relevant in my reflection.
https://www.verywellmind.com/what-is-flow-2794768
I took LinkedIn Learning Essentials of Team Collaboration course to increase quality of my reflection
https://bitbucket.org/vileider/semester1/src/main/images/TeamCollaborationCourse.png
At the end of my day I finished the reflection -which was not easy.

### Thursday 9.12.2021

I finished another hakkerrank chalange. I copied the code to my repository and at 10th attempt finally solved the problem.
https://bitbucket.org/vileider/semester1/src/main/python_coding/pythonTask2.py
I listened podcast about cyber security. https://www.youtube.com/watch?v=MAQxP1yidCo&t=2045s
I was gathering information for learning outcome 1.1.1.4. Podcast was interesting. In one sentence: I should not use one password for every account I have got.

### Friday 10.12.2021

I had videocall with Douglass, He explained to me how should I understand learning outcome 1.1.1.5.
I know how should provide evidence and how I should write programs in variety of languages.
After videocall I had a conversation with Tom about missing commits at portal. He explained how
should solve my problem and how should I fix possible errors in the future
While doing hakkerrank challenge I learned how to pass provided map object to a list in python
from https://www.journaldev.com/22960/python-map-function website.
I created JavaScript solution for the problem program described
at: https://www.hackerrank.com/challenges/list-comprehensions/problem?isFullScreen=true

### Monday 12.12.2021

I wrote a reflection about variety of languages that I have learned.

### Tuesday 15.12.2021

I had a pair programming session with my friend we tried to
get key name from object. We have found the solution at:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys

### Wednesday 16.12.2021

I wrote a reflection about security
I wrote reflection about Learning outcome 1.1.1.2

### Thursday 17.12.2021

I Submitted evidence for 1 semester

### Friday 18.12.2021

problem with object instance
ask for help
no solution for elegant way
solution with usage of object prototypes
###
###
### Second Semester
###
###
### Tuesday 11.1.2022
I worked with my friend on our project in react.
We finished score table.
Implementation of the code was not hard it was just repetition of already known knowledge.
https://github.com/szymonbasiul/bosoreact2/commit/8378851ae2a2dccca4ff000c0592ed8876b7a10f

### Wednesday 12.1.2022
I tried to add database to my shop-in-list.com project. 
There was no MySQL database at my hosting provider at nazwa.pl.
I had to choose other database so I choose PostgreSQL because it is relational.

### Thursday 13.1.2022
I was struggling with PostgreSQL on nazwa.pl for an hour. Database did not want to activate. I asked costumer service they requested screenshot.
I surrender that hosting service. It was not first time when I find part of their service hard to set up.
My account on AWS was still working so I finalized my account settings and I created MySQL database in 10 minutes.
I wanted to upload pictures to database so I am wondering what is the best solution. I am thinking about AWS s3, "cloud flare" or supabase.io 
Still I need to read about all solutions or watch some tutorials and then I will make my decision.

### Friday 14.1.2022

I have been working in pair with my friend under our project. We finished score count and prepared code for sending data to database.
We practiced connecting MySQL workbench to AWS database.
https://github.com/szymonbasiul/bosoreact2/commit/a49305a9a41d069405d4e4f37f0a631c6a7538ee

### Monday 15.1.2022
I have been watching "React with server side rendering" course. I need more time to understand how to set up everything.
Later I was working with my friend we were connecting our app to server, however data has been sent twice instead of once.
I need to check what cause that. 
https://github.com/szymonbasiul/bosoreact2/commit/e4a7b3a777341093207888c0de1d3a5ba57f0aee

### Tuesday 16.1.2021
I sort out double fetch problem by putting it inside on click function. However my solution triggers too many rerenders. I need to use memo or use callback to stop many rerenders. 

### Wednesday - Friday 17- 20.1.2022
I tried to solve the problem from Tuesday. I searched on google. I asked my friends. I asked on slack channel and I get many replies. Some of solution were about cleaning code some about fixing it. I red couple articles passed to me about cleaning code. They were useful and improved my coding skill but did not solved the problem. 
Finally one of the students send me article about strict mode. I search internet to learn as much as I can about it. I figured out how to turn this mode off. That was it.
The problem occurs only in development. It was React tool to help programmers finding minor code bugs. That experience taught me how to search in a more efficient way and that students are very helpful.
### Monday 24.1.2022

After 10 am I attend to online session we have been divided into small groups of students. We wrote what we found difficult during 1 semester. After an hour break students came back and have been writing their hopes and fears. Afternoon I had a pair programming session. I noticed that we get used to work with our scrum board. The agile way of working is helpful and simplify our work. We tried to pass state do grand child component and we succeed. We did not add types to our component. I made a decision to add that task to scrum board because doing too much can overwhelm us easly and let us to be burned out.   

### Tuesday 25.1.2022

At college Webex session I had to talk about security outcomes for semester 2. In my student group, when time has come to finish,
we present our conclusion to every student at the session. I have noticed that everyone struggles in the same way and have similar problems.
Later I continued my project in pair programming. We created another functional component and tried to use import styles in SCSS. We struggle to make it working even if we did it with the tutorial. I need to ask students at slack channel. 

### Wednesday 26.1.2022

I attend to guest lecture session with David Goodbrand. I made some notes from his lecture and I will try to use it for reflection.
At 4, I gather group of students and used jamboard to prepare us for new project.

### Thursday - Friday 27-28.2.2022

On the webex meeting, in divided groups, students have been creating ideas about new project.
Every student match himself to certain group, to project that was most sutable for him.
On a friday group meeting we started design thinking on mural.
another meeting to populate Jira board.

### Monday 31.1.2022

I attend toCharlotte Scott Lecture about competition with max prize 8000. I should write my own ide on this project.
We had a short meeting with the team and we created more tasks on Jira board.
Later Tom's lecture about overview of algorithms and data structures was a very difficult one. However I catch up at the end of lecture. I was supporting myself with google.
Afternoon I was working in pair programming under my project. We prepared component for fetch request.

### Tuesday 1.2.2022

Morning webex meeting let everyone to understand what catalogue structure that we will use. We discussed overall concept how we going to create our project. 
Later working under my private project we were posting data to server.
We tried to improve node express server by adding module type to packet.json. We struggle for couple minutes but we sort out every error when one of student help us.

### Wednesday 2.2.2022

Toms lecture
Team meeting -database
Guest lecture Florin Anton - about database
team meeting finishing database 

### Thursday 3.2.2022

Attend to Toms lecture about IP
At 2:30 we had a team meeting

### Friday 4.2.2022
Toms lecture about Reflection gave me lot of ideas how I should approach to reflection writing.
We had small break between Toms lectures so students team meeting.
It was not about any specific subject and we felt disorientated. We did not have time to move with our project and we tried to use our time together in most efficient way. We started discussing our thoughts about the project - that was part of toms "exercise in team" task suggestion.
Later on Toms lecture I wrote reflection
### Monday 7.2.2022
Toms session about Note taking at 10. It was pretty interesting I took lots of notes and I get inspired how to write notes, daily logs and reflection.I had webex meeting with my team. We had very long session -an 4 hours long with breaks. We manage to create frame for our project in two frameworks.
https://bitbucket.org/JuliaKloda/live_chat/commits/23c246ef987461b02092d9853ed148a7f15b0f1b

https://bitbucket.org/JuliaKloda/live_chat/commits/ebfc70fc9e173cfba14d1bda77402ff18f90a7f3
It was tough lesson for all of us. I am not sure if everyone are happy after working so long. Night session let me and my friend to finish Post request to database. We started cleaning our code but adding types is no so easy so end on that for today.
https://github.com/szymonbasiul/bosoreact2/commit/2568b3246018e97d76fdb05394529523da32c044

https://github.com/szymonbasiul/bosoreact2-server/commit/00d016647f923353b1a4d121854558b11714920a
### Tuesday 8.2.2022
At 12:00 Team meeting Karen helped us to improve the way we discuss
our goals and told what do we miss at standups. It is good to hear opinion from someone who sees the big picture.
After team meeting me and other student tried to implement socket to test server on node and react.
We have been struggling for 1 hour and we decided that we need a small break and we will come back with fresh mind.
After a break we asked another student and with 3 of us, we solved the problem within 15 minutes. 
another example when someone who is not involved into problem look at it from the different angle and helps to solve the problem.
https://bitbucket.org/JuliaKloda/live_chat/src/Socket_testing_on_react/
### Wednesday 9.2.2022
The Daily team meeting was improved by yesterdays stuff pointed by Karen. Thanks to that, team meeting took less time and everyone understood what
is goin on in the project. I am thinking our project is going to slow I might think later what can I do with that.
Afternoon I had a pair programming with 3 other students. We tried to understand every single line of socketiIO. I would like understand this
npm package enough to explain it to my whole team.
https://bitbucket.org/JuliaKloda/live_chat/commits/232e09b9d3735288bf523c3476445a37c177e1fe
At evening time I started Agile Project with 2 my friends. We started from design thinking and move to populating scrum board after.
### Thursday -Friday 10-11.2.2022
Meetings with team as usual, the work I did, I will and the blockers.
Recorded explanation of socketIO 
### Monday 14.2.2022
Daily meeting with end of the sprint retrospective
We recorded code review.
Evening pair programming session -polishing scss(animation)
### Tuesday 15.2.2022
Tom Lecture about tests in python.
Quick team meeting
Lecture about any concerns with matching learning outcomes
playing with scss animation
### Wednesday-Friday 16 - 18.2.2022
Daily meetings
Guest lecture with Barnaby Mercer, Maria Bel and Simone Ivan
Finishing the project for presentation meeting to prepare presentation
Presentation on Friday.
### Monday 23.2.2022
3 in group meeting
Trying to understand guest lecture with Florin
afternoon pair programming
### Tuesday 22.2.2022
Morning Lecture with Tom
Daily Team meeting
Finished Florin Anton Lecture.
Writing a reflection
afternoon pair programming - background animation
### Wednesday 23.2.2022
quick daily meeting
Guest lecture with Richard Carter
### Thursday - Friday 24-25.2.2022
I was ill
I recorded new way of using styles in our project
### Monday - Tuesday 28.2 - 1.3.2022
I wrote a reflection about working in a group
I wrote a reflection about threat model.
I attend to team meeting and Tom's lecture mainly about outcomes
I programmed in pair with my friend. we have been using scss to animate background.
### Wednesday 3.2.2022
I was working with one of team member on tests
we figured out that if we want to  write tests related to dark mode we need to
clean up the code a little bit.
After sorting out styles we were trying to to use variables in CSS
To remind how to do it I used this website
https://www.w3schools.com/css/css3_variables.asp
We used variables and we started adding test by installing "testcafe"s
from this website:
https://testcafe.io
We used "testcafe" documentation to test if we can check existence of the button.
Once we set up first test we started to write the test that would fail - checking if website background color is black.
We had to finish what we do and attend to daily team meeting.
Guest lecture with Douglas was very difficult but interesting.
At evening time I was working in pair under animation in our project
we were using w3schools and css-tricks websites to learn how to animate waves.
https://github.com/szymonbasiul/bosoreact2/commits/Szymon_branch
I read one chapter about using useMemo and useCallback from Robin Wieruch
"THE ROAD TO REACT" Book.
### Thursday 3.3.2022
I took an half an hour of writing on keyboard lesson from https://kurspisania.pl/
I was watching tutorial a trying to see the difference between useCallback and useMemo
https://www.youtube.com/watch?v=THL1OPn72vo&t=322s
https://www.youtube.com/watch?v=_AyFP5s69N4&t=326s
I was writing test for dark mode and creating dark mode on/off functionality 
https://bitbucket.org/JuliaKloda/live_chat/commits/065903cea4614d77bde688672f1d49e6b3b4ff58
I was googling how to create bat file which will run visual studio code and run server and frameworks in terminal
I was trying to finish reflection abut code review
### Friday 4.3.2022
I finished code review reflection
I read the article about code quality
I understood now two react hooks
use memo returns only the value from inside the function
use callback returns whole function and allows to pass arguments to the function
I was learning how to write on keyboard on https://kurspisania.pl/
Our team had code review session. 
After daily and code review we had a discussion about UI and unit testing.
We were looking for different testing frameworks.
I fount youtube video about right way of using hooks in react.
https://www.youtube.com/watch?v=UQkTu-PQ5gQ
### Monday 7.3.2022
Watching stored procedure tutorials
https://www.youtube.com/watch?v=NrBJmtD0kEw
I wrote reflection about quality of code
I attend on daily meeting, we discussed LO1.3.2.3 
I had a pair programming session with my friend we started new project
at first we had design thinking and user stories.
### Tuesday 8.3.2022
I watched youtube video about SQL Procedures https://www.youtube.com/watch?v=NrBJmtD0kEw
I read https://www.w3schools.com/sql/sql_stored_procedures.asp 
and I created simple stored procedures
### Wednesday 9.3.2022
I created procedures with parameter and after daily meeting I recorded explanation.
I attend to Armen guest lecture
I started next project with my friend, we populate scrum board and created 2 components
### Thursday 10.3.2022
after daily meeting I was adding dark-mode functionality in to team project(pair programming)
### Friday 11.3.2022
I have been finishing stored procedures on database
I modified react chat and server to use stored procedures on database
I had an issue with repository but as a team we solved that 
I added functionality at vue chat and I started refactoring code.
this tutorial was helpful:
https://michaelnthiessen.com/pass-function-as-prop/
this as well
https://youtu.be/qZXt1Aom3Cs
### Monday 14.3.2022
morning daily gave us more ideas what we can say on retrospective
At retrospective we discussed what we everyone have been struggling with.
I was watching tutorial about VUE
### Tuesday 15.3.2022
I attend  to daily meeting and a code review after.
I have been on on Q and A about Learning outcomes.
### Wednesday 16.3.2022
guest lecture with Caroline Laurenson
helping team member with register functionality
reading about aws amplify
creating register at private project
### Thursday 17.3.2022
I was Editing video recording and I was learning how use that tool.
I attend to Toms lecture about refactoring
Finished editing code review video
I attend to Daniel Krook guest lecture.
### Friday 18.3.2022
I finished editing code review recording.
I attend to daily meeting and code review session with tom and Karen
### Monday - Friday 20-25.3.2022
whole week daily standups
I was Learning about IBM and AWS cloud services
Upgrading project in pair programming
Writing first test in JEST.
### Monday - Tuesday 28-29.3.2022
I have been learning about AWS server less functions and gateways.
Evening time I am working under private project in pair programming.
### Wednesday- Friday 30.3 - 1.4.2022
I have been learning about AWS server less functions and gateways.
I was creating crud for private project
### Tuesday 19.4.2022
At all hands meeting I came to a conclusion that I should write more
stuff at daily log even small articles or youtube viedo.
After the meeting I had a video chat with one of the students to ask for
best practices of evidence gathering.
I wrote a reflection about personal data values.
### Wednesday 20.4.2022
I was working on server less functions at AWS
https://github.com/jeremydaly/serverless-mysql
I wrote reflection about testing
### Thursday 21.4.2022
I set up self assessment tool.
https://youtu.be/d_BCGvXbpKs
https://www.youtube.com/watch?v=xQKCpJnmLNM
I looked up on code analysis tool https://www.g2.com/categories/static-code-analysis/free
I finished reflection about code analysis
### Friday 22.4.2022
I was gathering information about security
I wrote a reflection about security
### Monday 25.4.2022
I have been working under UI of my portfolio in FIGMA.
Watching videos about styling good practices
https://www.youtube.com/watch?v=UWwNIMHFdW4&list=PL-LaT8ysl3eGGUXRiRG4sR91qoDlvW_Tq&index=11
I was gathering evidence for database
### Tuesday 26.4.2022
https://www.youtube.com/watch?v=MiAl2mQ718s
I created reflection about database
### Wednesday 27.4.2022
attended to guest lecture.
I recieved feedback from student
### Thursday 28.4.2022
I attend to Dev.js summit conference.
I gave feedback to one of the students.
I created reflection about feedback.
### Friday 29.4 - Friday 6.5.2022 
I have been working under Algorithm reflection most of the time.
It was the hardest outcome so far.
I managed to code in python and write some notes about it.
On Thursday I had a interesting guest lecture with Keith Burges.
I have been working on my private project with a friend.
We refresh our UI by using Figma tool.