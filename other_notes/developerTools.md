Visual studio code extensions:

- live share
- SCSS compiler
- prettier
- GitHub integration
- Python compiler
- Spell right
- Vetur
- Live Server
- Jira and Bitbucket Integration

Cloud services:

1. IBM
   - toolchain
   - cloud database
   - Node express hosting
2. AWS
   - server less functions
   - gateways
   - databases
   - CLI

Online apps/websites:

- https://imgbb.com
- https://github.com
- https://bitbucket.org
- https://codepen.io
- https://www.notion.so
- https://uhi.atlassian.net/jira
- https://trello.com
- https://linear.app
- https://www.youtube.com
- https://www.linkedin.com/learning
- https://jamboard.google.com
