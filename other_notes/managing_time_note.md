(MacCann, J. Fogarty and D.Roberts, 2011)
Conscientiousness as the most important factor of time management,
reveals in meeting deadlines, organization, and planning.

Proper time Managing comes from good habits or learnable behaviours.

Learning behaviours are achieved as part of a process
of increasing knowledge, training or deliberate practice.

Main source of stress:
* Bad work-time allocation
* "cramming for exams" (sic.)
* Failure to meet deadlines.


MacCann, C., J. Fogarty, G. and D.Roberts, R., 2011. Strategies for success in education.
[online] https://www.sciencedirect.com. Available at:
<https://www.sciencedirect.com/science/article/abs/pii/S1041608011001786> [Accessed 9 May 2022].