(Atto, 2022)

Good code quality is a result of multiple attributes like:
* Modularity:
    Blocks of functionality swapped in or out without causing fall of the whole application.
* Reusability:
    Parts of my code can be reused in other project.
* Maintainability
    The ease with which my code can be upgraded/altered over time without introducing new bugs
* Readability
    Easy to understand or follow logically.
* Test Driven Development
    Test cases that my code should be able to pass if it’s written correctly. 
* Automated Code Review tools
    Code review reveals which area of my code is most vulnerable in.




Atto, E., 2022. How to start writing high quality code at any point of your programming journey. [online] Medium. Available at: <https://medium.com/the-andela-way/how-to-start-writing-high-quality-code-at-any-point-of-your-programming-journey-d434cb0ba8ca> [Accessed 5 March 2022].