(Best Practices: How To Design a Database, 2022)
Database is a mass of information stored in framework

There are principles of well designed database:
* Designing database should be based on stakeholder expectations
* Choosing the right database. Making sure that I really need SQL one.
* Lowering database redundancy to minimum
* Transparent structure of database for better readability.
* Defining constrains prevent from bad data getting in.
* Documentation about designs and entity relationship for future users.
* Creating backups 
* Data Integrity and security(Todorow, 2022)
* By using database analyzer I Can optimize it.
* Keeping database on separate server lowers CPU usage and improves
security.




Conceptatech.com. 2022. Best Practices: How To Design a Database. [online] Available at: <https://www.conceptatech.com/blog/best-practices-how-to-design-a-database> [Accessed 26 April 2022].

Todorow, B., 2022. Cyber Security. [online] Bitbucket.org. Available at: <https://bitbucket.org/vileider/semester1/src/main/reflections/cyber_security_md> [Accessed 26 April 2022].