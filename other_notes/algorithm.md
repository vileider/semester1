Algorithm 
* Clearly defined problem statement, input and output
* The steps in the algorithm need to be in a very specific order
* The steps need to be distinct
* the algorithm should produce a result
* The algorithm should complete in finite amount of time

Good algorithm must balance between consumed memory and running time

When measuring algorithm efficiency I use worse case scenario.

Time complexity:
* Linear time O(n) 
* constant time O(1)
* quadratic time O(n^2)

Linear search Algorithm -on by one 1,2,3...
1. Start at the beginning
2. Compare current value to target
3. Move sequentially
4. Reach end of list

Binary search algorithm - divide array in a half and keep searching

Binary recursive algorithm calls itself.
Recursive depth -number of calls in recursive function.

Heterogeneous array -allow to insert any kind of data inside.
Homogenous array -only one type.

(Introduction to Big O Notation and Time Complexity, 2022)
O(n) -linear search
O(log n) -binary search

Auxiliary stack -structure that is used temporary to solve the problem
and is terminated when the problem is solved.

Python has amortized constant **Space complexity**
-memory allocate specific amount of space depending from stored data.
(0,4,8,16,25)

If I want to delete first item in array all items shifts and changes their index.

Linked list is a linear data structure which each element in the list is contained in 
separate object called Node.
Node modules contains two pieces of information:
* individual item that I want to store
* reference to another Node in a list
First node is called Head of the list.
Last one is called the Tail.
Tail does not reference to another Node because there is not any.

Linked list can be:
* Singly Linked List(each element pointing just on next one in a list)
* Doubly Linked List(each element reference to one before and after)

Inserting data to a list is more efficient than inserting to an array.

Tree data structure starts from Node called **Root Node**
"Top" Node can have multiple "branches" called **Nodes**.
Each Node can have multiple entities called **Leaf Nodes**

Root Node is at level 0, it is a parent for anything below.

###

(3 Levels of Sorting Algorithms, 2022)

Bubble sort - larger element goes to the top of the list
and swapping the if they are out of order.
Bubble sort performance are average, however it used as an introduction
to algorithms. Efficiency drastically drops as the number of elements increases.
* Performance O(N^2)
* Space complexity O(1)

Selection sort -Divides input list into two parts, sorted and unsorted.
Selects the smallest unsorted element, compare and swaps with the element at sorted part of the list.
* Performance O(N^2)
* Space complexity O(1)
It is similar to Bubble sort algorithm but it is slightly faster. 

Merge sort -takes input list and divides it over and over until it has lot of sub-list.
Compares the list of two elements then four, eight and so on, Until the whole list is sorted.
* Performance O(N Log N)
* Space Complexity O(N)

Tim sort- it iterates over a stack of items and collect items. Places them after on auxiliary stack
until some amount of items. Elements are added to it via binary search. When Tim sort criteria are met
it runs from stack and merge two of the together.
* Performance O(N Log N)
* Space complexity O(N)



Youtube.com. 2022. 3 Levels of Sorting Algorithms. [online] Available at:
<https://www.youtube.com/watch?v=qk7b4-iyCJ4> [Accessed 6 May 2022].

Youtube.com. 2022. Introduction to Big O Notation and Time Complexity. [online] Available at:
<https://www.youtube.com/watch?v=D6xkbGLQesk> [Accessed 6 May 2022].