4.5.2022
intervention styles

**customer** -one of transaction
**client** -might be long term relation

Heron model style

Prescriptive:
consultant directing/ telling customer what they want to do.
Something urgent.

Informative:
Clarify, explain

Confronting:
providing some feedback 

Catalytic:
encouraging another person.
questions short.

Cathartic:
Invite emotional expression
release tension.
emphatic. "I have notice you are struggling, what can make it right"
allow other to speak

Supportive:
building confidence


Task:
"Why did I use this style and what happened if I use it too much?"

Catalytic:
I prefer to give a rod and teach to fish instead of giving a fish.
I like extracting someone hidden skills to daylight
or expanding already known.
Person can become frustrated if the try will not go well.

Task:
"Why I am not comfortable with tis style"

* Depends who you are confronting and some people
* I take responsible for explanation and I might be understood in wrong way
* Don not like high lighting bad stuff or intention of feedback can be read as
    attempt of highlighting bad stuff.

Always focus on the object rather than a person -Tom McCallum