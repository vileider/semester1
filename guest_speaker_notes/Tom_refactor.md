Martin Fowler -refactoring book about good practice techniques

refactoring - improving structure of existing computer code without changing its external behavior

Once I have API I should think what is my REST

examples of refactor:
* moving the function that I use often into external component
* exactly the same thing I did before but in more readable way
* not adding extra arguments (it can affect other people's code)

Why?:
* when we want to add existing functionality
* when we want add new functionality but cannot due to earlier design decisions
Why not:
* lack of knowledge 
* short term project
* I am not paid to
* refactor might break the program (no test)


code smells decisions that make your code less
* extensible
* readable 
* maintainable
by others in the future


Future updates easier for me if I will refactor my code.
Googling refactor techniques for specific is a good idea.
I should be able to detect duplicate code to put in to function

Single responsibility method -good.
Long parameters - bad.
Test are the best to check if we are did refactoring in a good way.
Acceptance test - when I know what should be at beginning and at the end

Comments:
* first I should refactor before to write refactor
* when I do not know what to do
* god to say why you did something

