Guest Speaker - Florin Anton (IBM) talking about databases

Concurrency - Multiple users trying use data or have access to data.
Database transaction - operations which are executed into memory of the server(database).
Concurrency issues - multiple users trying get access to database
maximum concurrency + lot of issues OR minimum concurrency + minimum issues.

Transactions - sequence of SQL operations executed on buffer pool
manual: Commit or Rollback
auto-commit 
Initial state: the end of previous transaction

Lost update - when two transactions read and try to attempt update on the same data

Uncommitted Read - data read during transaction from memory pool(calculations might be do basing on rollback data)

Non-repeatable read -when two transaction read data and read data is different to each transaction(one transaction modify a row) 

Phantom read -  when two transaction read data and data has extra row/column or deleted row/column

Locks - limit or prevent data access by concurrent users or app process

Deadlock - when 2 or more transaction are waiting for each other 
and operation never finish

Deadlock detector - discovers deadlock and randomly terminate(rollback) the transaction until there will 
just one

Cursor - Tell where we are in the table

Isolation levels in DB2:
Repeatable read - locks the entire table.
Read stability - locks modified row or view.
Cursor stability - locks only the row referenced by the cursor, when we want see committed data
        a). currently committed - writer is not blocking the reader(reading straight from database)

Uncommitted read - locks only rows modified in transaction with Drop or Alter Table.
